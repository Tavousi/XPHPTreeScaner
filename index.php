<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <link href="css/dtree.css" rel="stylesheet" />
    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/dtree.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".dTree").dTree();
        });
    </script>
    <title>Jquery Directory Tree</title>
</head>

<body>
    <div class="dTree">
        <ul>
            <li><a href="#">List</a></li>
<?PHP
    // Original PHP code by Chirp Internet: www.chirp.com.au
    // Please acknowledge use of this code by including this header.
    function getFileList($dir, $recurse = false, $depth = false)
    {
        $retval = array();
        // add trailing slash if missing
        if (substr($dir, -1) != "/")
            $dir .= "/";
        // open pointer to directory and read list of files
        $d = @dir($dir) or die("getFileList: Failed opening directory $dir for reading");
        $i = 0;
        while (false !== ($entry = $d->read())) {
            // skip hidden files
            if ($entry[0] == ".")
                continue;
            if (is_dir("$dir$entry")) {
                $retval[$i] = array(
                    "name" => "$entry",
					"patch" => "$dir$entry",
                    "type" => 'dir',
                    "size" => 0,
                    "lastmod" => filemtime("$dir$entry"),
                    "sub" => array()
                );
                if ($recurse && is_readable("$dir$entry/")) {
                    if ($depth === false) {
                        $retval[$i]['sub'] = array_merge($retval[$i]['sub'], getFileList("$dir$entry/", true));
                    } elseif ($depth > 0) {
                        $retval[$i]['sub'] = array_merge($retval[$i]['sub'], getFileList("$dir$entry/", true, $depth - 1));
                    }
                }
            } elseif (is_readable("$dir$entry")) {
                $retval[$i] = array(
                    "name" => "$entry",
					"patch" => "$dir$entry",
                    "type" => mime_content_type("$dir$entry"),
                    "size" => filesize("$dir$entry"),
                    "lastmod" => filemtime("$dir$entry")
                );
            }
            $i++;
        }
        $d->close();
        return $retval;
    }
    function fo($v)
    {
        if (is_array($v)) {
            echo '<ul>';
            foreach ($v as $k => $v) {
        echo '<li><a href="#" data-url="'.$v['patch'].'" data-lastmod="'.$v['lastmod'].'"'.(($v['type'] != 'dir') ? ' data-type="'.$v['type'].'" data-size="'.$v['size'].'"' : ' data-type="dir"').'>' . $v['name'] . '</a>';
                fo($v['sub']);
            }
            echo '</ul>';
        }
    }
    $dirlist = getFileList("C:\WampDeveloper\Websites\www.forum.in\webroot", true, 5);
    foreach ($dirlist as $k => $v) {
        echo '<li><a href="#" data-url="'.$v['patch'].'" data-lastmod="'.$v['lastmod'].'"'.(($v['type'] != 'dir') ? ' data-type="'.$v['type'].'" data-size="'.$v['size'].'"' : ' data-type="dir"').'>' . $v['name'] . '</a>';
        fo($v['sub']);
        echo '</li>';
    }
?>
        </ul>
    </div>
</body>

</html>